from flask import Flask,send_file,request
app = Flask(__name__)
from SimpleCV import Camera

cam = Camera()

@app.route('/')
def SendImage():

    newImg = cam.getImage()
    newImg.save('camimage.jpg')
    return send_file('camimage.jpg', mimetype='image/jpg')

if __name__ == '__main__':
    app.run(host='0.0.0.0') #everyone is allowed to access my Server 
