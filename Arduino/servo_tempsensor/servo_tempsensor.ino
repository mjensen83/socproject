#include <Servo.h> 
#include <Serial.h>
#include <OneWire.h>

// SG90 Servos
Servo myservo;  // create servo object to control servo 1
Servo myservo2; // create servo object to control servo 2

#define pinServo A0 //Definerer en variabel til pin A0
#define pinServo2 A1 //Definerer en variabel til pin A0

// DS18S20 Temperature chip i/o
OneWire ds(10);  // on pin 10

void setup() 
{ 
	Serial.begin(9600);

	// attaches the servo on pin to the servo object
	myservo.attach(pinServo);
        myservo2.attach(pinServo2);

	// init angle of servo inbetween two limitations
        myservo.write(90);
        myservo2.write(90);
        delay(250);
        myservo.detach();
        myservo2.detach();
       
} 

int HighByte, LowByte, TReading, SignBit, Tc_100, Whole, Fract;
char buf[20];
int servoPos, servo2Pos;

void loop() 
{ 	
        int angle = myservo.read();
        int angle2 = myservo2.read();
        
        
        // Serial read
        if( Serial.available() ) 
        {
          
          myservo.attach(pinServo);
          myservo2.attach(pinServo2);
   
          servoPos = Serial.parseInt();
          servo2Pos = Serial.parseInt();
          
          myservo.write(servoPos);
          myservo2.write(servo2Pos);
          delay(250);
          myservo.detach();
          myservo2.detach();    
        }
   
///////////////////Temp sensor code/////////////////////
        byte i, sensor;
        byte present = 0;
        byte data[12];
        byte addr[8];
        byte type_s;
        float celsius;

        if ( !ds.search(addr)) {
            //Serial.print("No more addresses.\n");
          ds.reset_search();
          return;
        }

        //Serial.print("R=");
        for( i = 0; i < 8; i++) {
        //Serial.print(addr[i], HEX);
        // Serial.print(" ");
        }

        if ( OneWire::crc8( addr, 7) != addr[7]) {
         // Serial.print("CRC is not valid!\n");
        return;
        }

        if ( addr[0] == 0x10) {
         //Serial.print("Device is a DS18S20 family device.\n");
         type_s = 1;
        }
        else if ( addr[0] == 0x28) {
        //Serial.print("Device is a DS18B20 family device.\n");
        }
        else {
        //Serial.print("Device family is not recognized: 0x");
        //Serial.println(addr[0],HEX);
        return;
        }

        ds.reset();
        ds.select(addr);
        ds.write(0x44,1);         // start conversion, with parasite power on at the end

        delay(750);     // maybe 750ms is enough, maybe not
        // we might do a ds.depower() here, but the reset will take care of it.

        present = ds.reset();
        ds.select(addr);    
        ds.write(0xBE);         // Read Scratchpad
  
        //Serial.print("P=");
        //Serial.print(present,HEX);
        //Serial.print(" ");
        for ( i = 0; i < 9; i++) {           // we need 9 bytes
          data[i] = ds.read();
        }
  
        // Convert the data to actual temperature
        
        unsigned int raw = (data[1] << 8) | data[0];
        if (type_s)
        {
         raw = raw << 3; // 9 bit resolution default
         if (data[7] == 0x10)
         {
          // count remaining gives full 12 bit resolution
         raw =(raw & 0xFFF0) + 12 - data[6]; 
         }
        }
        else
        {
          byte cfg = (data[4] & 0x60);
          if (cfg == 0x00) raw = raw << 3; // 9 bit resolution, 93.75 ms
          else if (cfg == 0x20) raw = raw << 2; // 10 bit res, 187.5 ms
          else if (cfg == 0x40) raw = raw << 1; // 11 bit res, 375 ms
           // default is 12 bit resoultion, 750 ms conversion time 
        }
        celsius = (float)raw / 16.0;
        Serial.println(celsius); 
    }
   
    

